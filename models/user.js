var mongoose = require('mongoose');

var tagSchema = new mongoose.Schema({
    text: String
});

var bookmarkSchema = new mongoose.Schema({
    _id: {
        type: Number,
        required: true
    },
    url: {
        type: String,
        required: true
    },
    category: {
        type: Number,
        required: true
    },
    name: String,
    description: String,
    date: Date,
    tags: [tagSchema],
    is_favorite: Boolean
});

var categorySchema = new mongoose.Schema({
    _id: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    parent: {
        type: Number
    },
    children: [Number]
});

var userSchema = new mongoose.Schema({
    oauth_id: String,
    name: String,
    email: String,
    bookmarks: [bookmarkSchema],
    categories: [categorySchema],
    counter: Number,
    bid: Number
});


userSchema.statics.insertBookmark = function (userId, newBookmark, cb) {
        this.update({_id: userId}, {$inc: {bid: 1}, $push: {bookmarks: newBookmark}}, cb);
};



module.exports.UserSchema = userSchema;