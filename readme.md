TODO: deployment manual
#Deployment manual
Please follow this manual if you want to get Personal Web Clipper properly running on your server

##1. Requirements
OS:
- Windows 7 SP1 or higher
- Windows Server 2008 R2 or higher
- Ubuntu 12.04.5 or any superior LTS release

Software:
- MongoDB 3.2 or higher
- Node 5.9.0 or higher
- GDI+ library for Windows or libfont2 for Ubuntu (required for proper snapshot rendering)
- PM2 [npm package] (required for running node applications as daemons)

##2. Pre-configuration
###2.1 Auth tokens
You will need to register your own OAuth access tokens for using social networks authentication.
Follow those links to get your own tokens:
- Google - https://developers.google.com/identity/protocols/OAuth2#basicsteps
- Twitter - https://dev.twitter.com/oauth/overview
- Facebook - https://developers.facebook.com/docs/facebook-login/access-tokens
After you have claimed your tokens, please modify ./config/auth.config.js and paste your tokens data there.
IMPORTANT! Make sure that callback url is set up the proper way, otherwise it will be impossible to authorize users on your servers.

###2.2 Set up DB
After you have successfully run your MongoDB daemon you should change database connection strings.
Open ./config/db.config.js and set your own connection strings.
Note, that snapshot service should use its own collection and occupy some space on the hard drive for storing images. Thus consider moving snapshot service onto separate server.

##3 Deployment
The most convenient method is to deploy node applications using PM2 utility

