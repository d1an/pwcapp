var should = require("should");

var bookmarksLogics = require("../routes/bookmarksLogics");
var categoriesLogics = require("../routes/categoriesLogics");
var mocks = require('node-mocks-http');
var db = require('../config/configurator');
var User = db.User;
var req = require('./util').req;

describe("example", function() {
   it("should calc 2+2", function(done) {
      (2+2).should.eql(4);
      done();
   });
   
   it("should have proper env", function(done) {
      process.env.NODE_ENV.should.eql("test");
      done();
   });
   
   it("should get a user",function(done) {
       User.findOne({}, function(err, doc) {
         should.not.exist(err);
         doc.name.should.eql("Ананас Апельсинович");
         done();
       });
   });

    it("should get bookmarks list", function (done) {
        //res = mocks.createResponse();
        bookmarksLogics.getBookmarks(req(), {
            send: function (data) {
                data.should.not.eql(null);
                done();
            }
        });
    });
    
    it("should add a bookmark", function (done) {
        var r = req();
        r.body = {url: "SIEG HEIL", category: 0};
        bookmarksLogics.postBookmarksAdd(r, {
            sendStatus: function (data) {
                data.should.eql(200);
                done();
            }
        });
    });
});