// Import should for unit tests
var should = require("should");
var mongoose = require('mongoose');

// Import our methods
var bookmarksLogics = require("../routes/bookmarksLogics");
var categoriesLogics = require('../routes/categoriesLogics');
var mocks = require('node-mocks-http');

// Import db
var mongo = require('mongodb');
var ObjectId = mongo.ObjectID;
//var monk = require('monk');
//var dbmock = monk('pwcdb.cloudapp.net/test'); // Connection string for testing db here


// working with db
//var dbmock = mongoose.connect('mongodb://pwcdb.cloudapp.net/test');
var dbmock = require('../config/dbmock.js');

var User = dbmock.User;

// Global mocks for req and res
var req, res, next;

// Create a response object with built-in event emitter (needed for async calls)
function buildResponse() {
  return mocks.createResponse({eventEmitter: require('events').EventEmitter});
}

//Date 
//var d = new Date();
//var ds = "";

describe("Bookmarks methods", function() {

  beforeEach(function(done) {
    // DB reset can be slow, so extend timeout
    this.timeout(10000);
    
    // Reset REQ and RES mocks
    req = {
      //db: dbmock,
      session: {
        passport: {
          user: {
            _id: ObjectId("566b37820fa2ab923a0c7b80") 
          }
        }
      },
      params: null,
      body: null
    };
    res = buildResponse();
    

    // Refresh the testing db
    require("../test/util.js").clean(User, req.session.passport.user._id, done);
  });

  describe("GET bookmarks list", function() {
    this.timeout(10000);
    
    it("should return 200 if everything is ok", function(done) {
      // Event handler
      res.on("end", function() {
        res._getStatusCode().should.eql(200);
        res._getData().should.not.eql(null);
        done();
      });
      
      // Method invoke
      bookmarksLogics.getBookmarks(req, res);
    });
    it("should return 400 when user doesn't exist", function(done) {
      req = {
      //db: dbmock,
      session: {
        passport: {
          user: {
            _id: ObjectId("576b37828fa2ab922a0c7b81") 
          }
        }
      }
      };
      res.on("end", function() {
        res._getStatusCode().should.eql(400);
        done();
      });
      bookmarksLogics.getBookmarks(req, res, next);
    });

    it("should return 401 when user unauthorised", function(done) {
      // Set request parameters
      req.session.passport.user = null;
      
      // Event handler
      res.on("end", function() {
        res._getStatusCode().should.eql(401);
        res._getData().should.eql("Unauthorized");
        done();
      });
      
      // Method invoke
      bookmarksLogics.getBookmarks(req, res);
    });
  });
  
  // GET /bookmarks/
  describe("GET search query", function() {
    this.timeout(10000);
    
    it("should return 1 bookmark when search query is 'ab'", function(done) {
      req.params = {
        query: "ab"
      };
      res.on("end", function() {
        res._getStatusCode().should.eql(200);
        res._getData().length.should.eql(1);
        done();
      });
      bookmarksLogics.getBookmarksSearch(req, res, next);
    });
    
    it("should return 1 bookmark when search query is 'AB'(UPPERCASE)", function(done) {
      req.params = {
        query: "AB"
      };
      res.on("end", function() {
        res._getStatusCode().should.eql(200);
        res._getData().length.should.eql(1);
        done();
      });
      bookmarksLogics.getBookmarksSearch(req, res, next);
    });
    
    it("should return 0 bookmark when search query is 'abc'", function(done) {
      req.params = {
        query: "abc"
      };
      res.on("end", function() {
        res._getStatusCode().should.eql(200);
        res._getData().length.should.eql(0);
        done();
      });
      bookmarksLogics.getBookmarksSearch(req, res, next);
    });
    
    it("should return 400 when user doesn't exist", function(done) {
      req = {
      //db: dbmock,
      session: {
        passport: {
          user: {
            _id: ObjectId("576b37828fa2ab922a0c7b81") 
          }
        }
      }
      };
      res.on("end", function() {
        res._getStatusCode().should.eql(400);
        done();
      });
      bookmarksLogics.getBookmarksSearch(req, res, next);
    });
    
    it("should return 401 when user unauthorised", function(done) {
      req = {
      //db: dbmock,
      session: {
        passport: {
          user: null
        }
      }
      };
      res.on("end", function() {
        res._getStatusCode().should.eql(401);
        done();
      });
      bookmarksLogics.getBookmarksSearch(req, res, next);
    });
  });
  
  // GET /bookmarks/tags/search/
  describe("GET search tags query", function() {
    this.timeout(10000);
    
    it("should return 1 tag when search query is 'football'", function(done) {
      req.params = {
        query: "football"
      };
      res.on("end", function() {
        res._getStatusCode().should.eql(200);
        res._getData().length.should.eql(1);
        done();
      });
      bookmarksLogics.getBookmarksTagsSearch(req, res, next);
    });
    
    it("should return 1 tag when search query is 'FOOTBALL'(UPPERCASE)", function(done) {
      req.params = {
        query: "FOOTBALL"
      };
      res.on("end", function() {
        res._getStatusCode().should.eql(200);
        res._getData().length.should.eql(1);
        done();
      });
      bookmarksLogics.getBookmarksTagsSearch(req, res, next);
    });
    
    it("should return 0 tag when search query is 'asd'", function(done) {
      req.params = {
        query: "abc"
      };
      res.on("end", function() {
        res._getStatusCode().should.eql(200);
        res._getData().length.should.eql(0);
        done();
      });
      bookmarksLogics.getBookmarksTagsSearch(req, res, next);
    });
    
    it("should return 400 when user doesn't exist", function(done) {
      req = {
      //db: dbmock,
      session: {
        passport: {
          user: {
            _id: ObjectId("576b37828fa2ab922a0c7b81") 
          }
        }
      }
      };
      res.on("end", function() {
        res._getStatusCode().should.eql(400);
        done();
      });
      bookmarksLogics.getBookmarksTagsSearch(req, res, next);
    });
    
    it("should return 401 when user unauthorised", function(done) {
      req = {
      //db: dbmock,
      session: {
        passport: {
          user: null
        }
      }
      };
      res.on("end", function() {
        res._getStatusCode().should.eql(401);
        done();
      });
      bookmarksLogics.getBookmarksTagsSearch(req, res, next);
    });
  });
  
  // Tests for GET /bookmarks/category/
  describe("GET bookmarks for category", function() {
    this.timeout(10000);
    
    it("should return 2 bookmarks when category id = 0", function(done) {
      req.params = {
        id: 0
      };
      res.on("end", function() {
        res._getStatusCode().should.eql(200);
        res._getData().length.should.eql(2);
        done();
      });
      bookmarksLogics.getBookmarksCategory(req, res, next);
    });
    
    it("should return 0 bookmark when category id = 8", function(done) {
      req.params = {
        id: 8
      };
      res.on("end", function() {
        res._getData().length.should.eql(0);
        done();
      });
      bookmarksLogics.getBookmarksCategory(req, res, next);
    });
    
    it("should return 400 when user doesn't exist", function(done) {
      req = {
      //db: dbmock,
      session: {
        passport: {
          user: {
            _id: ObjectId("576b37828fa2ab922a0c7b81") 
          }
        }
      }
      };
      res.on("end", function() {
        res._getStatusCode().should.eql(400);
        done();
      });
      bookmarksLogics.getBookmarksCategory(req, res, next);
    });
    
    it("should return 401 when user unauthorised", function(done) {
      req = {
      //db: dbmock,
      session: {
        passport: {
          user: null
        }
      }
      };
      res.on("end", function() {
        res._getStatusCode().should.eql(401);
        done();
      });
      bookmarksLogics.getBookmarksCategory(req, res, next);
    });
  });
  
  // GET /bookmarks/favorite
  describe("GET favorite bookmarks", function() {
    this.timeout(10000);
    
    it("should return 3 bookmarks", function(done) {
      res.on("end", function() {
        res._getStatusCode().should.eql(200);
        res._getData().length.should.eql(3);
        done();
      });
      bookmarksLogics.getFavorites(req, res, next);
    });
    
    it("should return 400 when user doesn't exist", function(done) {
      req = {
      //db: dbmock,
      session: {
        passport: {
          user: {
            _id: ObjectId("576b37828fa2ab922a0c7b81") 
          }
        }
      }
      };
      res.on("end", function() {
        res._getStatusCode().should.eql(400);
        done();
      });
      bookmarksLogics.getFavorites(req, res, next);
    });
    
    it("should return 401 when user unauthorised", function(done) {
      req = {
      //db: dbmock,
      session: {
        passport: {
          user: null
        }
      }
      };
      res.on("end", function() {
        res._getStatusCode().should.eql(401);
        done();
      });
      bookmarksLogics.getFavorites(req, res, next);
    });
  });
  
  describe("POST bookmarks add", function() {
    this.timeout(10000);
    
    it("should return 200 if bookmark is successfully added", function(done) {
      req.body = {
        "url" : "http://leagueoflegends.com/",
        "category" : 0,
        "name" : "LOL",
        "description" : null,
        "tags" : null
      };
      res.on("end", function() {
        res._getStatusCode().should.eql(200);
        done();
      });
      bookmarksLogics.postBookmarksAdd(req, res, next);
    });
    
    it("should return 401 when user is unauthorised", function(done) {
      req.body = {
        "url" : "http://leagueoflegends.com/",
        "category" : 0,
        "name" : "LOL",
        "description" : null,
        "tags" : null
      };
      req.session.passport.user = null;
      res.on("end", function() {
        res._getStatusCode().should.eql(401);
        done();
      });
      bookmarksLogics.postBookmarksAdd(req, res, next);
    });
    
    it("should return status 400 when user tries to add a bookmark with empty link", function(done) {
      req.body = {
        "url" : "",
        "category" : 0,
        "name" : "LOL",
        "description" : null,
        "tags" : null
      };
      res.on("end", function() {
        res._getStatusCode().should.eql(400);
        done();
      });
      bookmarksLogics.postBookmarksAdd(req, res, next);
    });
    
    it("should return status 400 when user does not exist", function(done) {
      req.body = {
        "url" : "http://lol.ua",
        "category" : 0,
        "name" : "LOL",
        "description" : null,
        "tags" : null
      };
      req.session.passport.user._id = ObjectId("576b37828fa2ab922a0c7b81");
      res.on("end", function() {
        res._getStatusCode().should.eql(400);
        done();
      });
      bookmarksLogics.postBookmarksAdd(req, res, next);
    });
  });
  
  describe("PUT bookmarks edit", function() {
    this.timeout(10000);
    
    it("should return 200 if bookmark is successfully edited", function(done) {
      req.body = {
        "url" : "http://leagueoflegends.com/",
        "category" : 0,
        "name" : "LOL",
        "description" : null,
        "tags" : null
      };
      req.params = {
        bid: 1
      };
      res.on("end", function() {
        res._getStatusCode().should.eql(200);
        done();
      });
      bookmarksLogics.putBookmarks(req, res, next);
    });
    
    it("should return 401 when user is unauthorised", function(done) {
      req.body = {
        "url" : "http://leagueoflegends.com/",
        "category" : 0,
        "name" : "LOL",
        "description" : null,
        "tags" : null
      };
      req.params = {
        bid: 1
      };
      req.session.passport.user = null;
      res.on("end", function() {
        res._getStatusCode().should.eql(401);
        done();
      });
      bookmarksLogics.putBookmarks(req, res, next);
    });
    
    it("should return status 400 when user tries to edit a bookmark with empty link", function(done) {
      req.body = {
        "url" : "",
        "category" : 0,
        "name" : "LOL",
        "description" : null,
        "tags" : null
      };
      req.params = {
        bid: 1
      };
      res.on("end", function() {
        res._getStatusCode().should.eql(400);
        done();
      });
      bookmarksLogics.putBookmarks(req, res, next);
    });
    
    it("should return status 400 when user does not exist", function(done) {
      req.body = {
        "url" : "http://lol.ua",
        "category" : 0,
        "name" : "LOL",
        "description" : null,
        "tags" : null
      };
      req.params = {
        bid: 1
      };
      req.session.passport.user._id = ObjectId("576b37828fa2ab922a0c7b81");
      res.on("end", function() {
        res._getStatusCode().should.eql(400);
        done();
      });
      bookmarksLogics.putBookmarks(req, res, next);
    });
  });
  
  
  describe("PUT set favorite", function() {
    this.timeout(10000);
    
    it("should return 200 if bookmark is successfully marked as favorite(or contrariwise)", function(done) {
      req.body = {
        "is_favorite": true
      };
      req.params = {
        bid: 1
      };
      res.on("end", function() {
        res._getStatusCode().should.eql(200);
        done();
      });
      bookmarksLogics.setFavorite(req, res, next);
    });
    
    it("should return 401 when user is unauthorised", function(done) {
      req.body = {
        "is_favorite": true
      };
      req.params = {
        bid: 1
      };
      req.session.passport.user = null;
      res.on("end", function() {
        res._getStatusCode().should.eql(401);
        done();
      });
      bookmarksLogics.setFavorite(req, res, next);
    });
    
    it("should return status 400 when user does not exist", function(done) {
      req.body = {
        "is_favorite": true
      };
      req.params = {
        bid: 1
      };
      req.session.passport.user._id = ObjectId("576b37828fa2ab922a0c7b81");
      res.on("end", function() {
        res._getStatusCode().should.eql(400);
        done();
      });
      bookmarksLogics.setFavorite(req, res, next);
    });
  });
  
  describe("DELETE delete bookmark", function() {
    this.timeout(10000);
    
    it("should return 200 if bookmark has deleted", function(done) {
      req.params = {
        bid: 1
      };
      res.on("end", function() {
        res._getStatusCode().should.eql(200);
        done();
      });
      bookmarksLogics.deleteBookmarks(req, res, next);
    });
    
    it("should return 401 when user is unauthorised", function(done) {
      req.params = {
        bid: 1
      };
      req.session.passport.user = null;
      res.on("end", function() {
        res._getStatusCode().should.eql(401);
        done();
      });
      bookmarksLogics.deleteBookmarks(req, res, next);
    });
    
    it("should return status 400 when user does not exist", function(done) {
      req.params = {
        bid: 1
      };
      req.session.passport.user._id = ObjectId("576b37828fa2ab922a0c7b81");
      res.on("end", function() {
        res._getStatusCode().should.eql(400);
        done();
      });
      bookmarksLogics.deleteBookmarks(req, res, next);
    });
  });
  
  describe("POST bookmarks extention add", function() {
    this.timeout(10000);
    
    it("should return 200 if bookmark is successfully added", function(done) {
      req.body = {
        "url" : "http://leagueoflegends.com/"
      };
      res.on("end", function() {
        res._getStatusCode().should.eql(200);
        done();
      });
      bookmarksLogics.postBookmarksExtensionAdd(req, res, next);
    });
    
    it("should return 401 when user is unauthorised", function(done) {
      req.body = {
        "url" : "http://leagueoflegends.com/"
      };
      req.session.passport.user = null;
      res.on("end", function() {
        res._getStatusCode().should.eql(401);
        done();
      });
      bookmarksLogics.postBookmarksExtensionAdd(req, res, next);
    });
    
    it("should return status 400 when user tries to add a bookmark with empty link", function(done) {
      req.body = {
        "url" : ""
      };
      res.on("end", function() {
        res._getStatusCode().should.eql(400);
        done();
      });
      bookmarksLogics.postBookmarksExtensionAdd(req, res, next);
    });
    
    it("should return status 400 when user does not exist", function(done) {
      req.body = {
        "url" : "http://lol.ua"
      };
      req.session.passport.user._id = ObjectId("576b37828fa2ab922a0c7b81");
      res.on("end", function() {
        res._getStatusCode().should.eql(400);
        done();
      });
      bookmarksLogics.postBookmarksExtensionAdd(req, res, next);
    });
  });

});


// Tests for categories logics
describe("Categories methods", function() {

  beforeEach(function(done) {
    // DB reset can be slow, so extend timeout
    this.timeout(10000);
    
    // Reset REQ and RES mocks
    req = {
      //db: dbmock,
      session: {
        passport: {
          user: {
            _id: ObjectId("566b37820fa2ab923a0c7b80") 
          }
        }
      },
      params: null,
      body: null
    };
    res = buildResponse();
    // Refresh the testing db
    require("../test/util.js").clean(dbmock, req.session.passport.user._id, done);
  });
  
  describe("GET categories list", function() {
    this.timeout(10000);
    
    it("should return 200 if everything is ok", function(done) {
      // Event handler
      res.on("end", function() {
        res._getStatusCode().should.eql(200);
        res._getData().should.not.eql(null);
        done();
      });
      
      // Method invoke
      categoriesLogics.getCategories(req, res);
    });
    it("should return 400 when user doesn't exist", function(done) {
      req = {
      //db: dbmock,
      session: {
        passport: {
          user: {
            _id: ObjectId("576b37828fa2ab922a0c7b81") 
          }
        }
      }
      };
      res.on("end", function() {
        res._getStatusCode().should.eql(400);
        done();
      });
      categoriesLogics.getCategories(req, res, next);
    });

    it("should return 401 when user unauthorised", function(done) {
      // Set request parameters
      req.session.passport.user = null;
      
      // Event handler
      res.on("end", function() {
        res._getStatusCode().should.eql(401);
        res._getData().should.eql("Unauthorized");
        done();
      });
      
      // Method invoke
      categoriesLogics.getCategories(req, res);
    });
  });
  
  describe("POST categoriess add", function() {
    this.timeout(10000);
    
    it("should return 200 if category is successfully added", function(done) {
      req.body = {
        "name" : "Funny folder",
        "parent": 0
      };
      res.on("end", function() {
        res._getStatusCode().should.eql(200);
        done();
      });
       categoriesLogics.postCategoriesAdd(req, res, next);
    });
    
    it("should return 401 when user is unauthorised", function(done) {
      req.body = {
        "name" : "Funny folder1",
        "parent": 0
      };
      req.session.passport.user = null;
      res.on("end", function() {
        res._getStatusCode().should.eql(401);
        done();
      });
      categoriesLogics.postCategoriesAdd(req, res, next);
    });
    
    it("should return status 400 when user tries to add a category with name, that already exist in this parent", function(done) {
      req.body = {
        "name" : "Videos",
        "parent": 0
      };
      res.on("end", function() {
        res._getStatusCode().should.eql(400);
        done();
      });
      categoriesLogics.postCategoriesAdd(req, res, next);
    });
    
    it("should return status 400 when user does not exist", function(done) {
      req.body = {
        "name" : "Funny folder2",
        "parent": 0
      };
      req.session.passport.user._id = ObjectId("576b37828fa2ab922a0c7b81");
      res.on("end", function() {
        res._getStatusCode().should.eql(400);
        done();
      });
      categoriesLogics.postCategoriesAdd(req, res, next);
    });
  });
  
  describe("PUT categories edit", function() {
    this.timeout(10000);
    
    it("should return 200 if category is successfully edited", function(done) {
      req.body = {
        "name" : "Funny folder2",
        "parent": -1
      };
      req.params = {
        id: 3
      };
      res.on("end", function() {
        res._getStatusCode().should.eql(200);
        done();
      });
      categoriesLogics.putCategoriesEdit(req, res, next);
    });
    
    it("should return 401 when user is unauthorised", function(done) {
      req.body = {
        "name" : "Funny folder2",
        "parent": -1
      };
      req.params = {
        id: 3
      };
      req.session.passport.user = null;
      res.on("end", function() {
        res._getStatusCode().should.eql(401);
        done();
      });
      categoriesLogics.putCategoriesEdit(req, res, next);
    });
    
    it("should return status 400 when user does not exist", function(done) {
      req.body = {
        "name" : "Funny folder2",
        "parent": -1
      };
      req.params = {
        id: 3
      };
      req.session.passport.user._id = ObjectId("576b37828fa2ab922a0c7b81");
      res.on("end", function() {
        res._getStatusCode().should.eql(400);
        done();
      });
      categoriesLogics.putCategoriesEdit(req, res, next);
    });
  });
  
  describe("DELETE delete category", function() {
    this.timeout(10000);
    
    it("should return 200 if category has deleted", function(done) {
      req.params = {
        id: 3
      };
      res.on("end", function() {
        res._getStatusCode().should.eql(200);
        done();
      });
      categoriesLogics.deleteCategories(req, res, next);
    });
    
    it("should return 401 when user is unauthorised", function(done) {
      req.params = {
        id: 3
      };
      req.session.passport.user = null;
      res.on("end", function() {
        res._getStatusCode().should.eql(401);
        done();
      });
      categoriesLogics.deleteCategories(req, res, next);
    });
    
    it("should return status 400 when user does not exist", function(done) {
      req.params = {
        id: 3
      };
      req.session.passport.user._id = ObjectId("576b37828fa2ab922a0c7b81");
      res.on("end", function() {
        res._getStatusCode().should.eql(400);
        done();
      });
      categoriesLogics.deleteCategories(req, res, next);
    });
  });
  
  describe("GET categories tree", function() {
    this.timeout(10000);
    
    it("should return 200 if everything is ok", function(done) {
      // Event handler
      res.on("end", function() {
        res._getStatusCode().should.eql(200);
        res._getData().should.not.eql(null);
        done();
      });
      
      // Method invoke
      categoriesLogics.getCategoriesTree(req, res);
    });
    it("should return 400 when user doesn't exist", function(done) {
      req = {
      //db: dbmock,
      session: {
        passport: {
          user: {
            _id: ObjectId("576b37828fa2ab922a0c7b81") 
          }
        }
      }
      };
      res.on("end", function() {
        res._getStatusCode().should.eql(400);
        done();
      });
      categoriesLogics.getCategoriesTree(req, res, next);
    });

    it("should return 401 when user unauthorised", function(done) {
      // Set request parameters
      req.session.passport.user = null;
      
      // Event handler
      res.on("end", function() {
        res._getStatusCode().should.eql(401);
        res._getData().should.eql("Unauthorized");
        done();
      });
      
      // Method invoke
      categoriesLogics.getCategoriesTree(req, res);
    });
  });
});