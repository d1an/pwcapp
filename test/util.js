var requestMock = {
    session: {
        passport: {
            user: {
                _id: "571f888189fd66b80fa25063"
            }
        }
    },
    params: null,
    body: null
};

function clean(user, id, callback) {
   user.update({"_id": id}, 
      {
   $set: {
     "categories": [
       {
               "name" : "Unsorted",
               "_id" : 0,
               "parent" : null,
               "children" : null
       },
       {
               "name" : "Favorite",
               "_id" : 1,
               "parent" : null,
               "children" : null
       },
       {
               "name" : "Music",
               "_id" : 2,
               "parent" : null,
               "children" : [
                       4,
                       5
               ]
       },
       {
               "name" : "Videos",
               "_id" : 3,
               "parent" : null,
               "children" : [ ]
       },
       {
               "name" : "Pop",
               "_id" : 4,
               "parent" : 2,
               "children" : [ ]
       },
       {
               "name" : "Rock",
               "_id" : 5,
               "parent" : 2,
               "children" : [ ]
       }
     ],
     "bookmarks": [
       {
               "url" : "http://terrikon.com/football/ukraine/championship/",
               "category" : 0,
               "name" : "terrikon.com/football/ukraine/championship/",
               "description" : null,
               "date" : "11.12.2015",
               "_id" : 1,
               "tags" : [
                       {
                               "text" : "football"
                       }
               ],
               "is_favorite" : true
       },
       {
               "url" : "http://pwcapp.azurewebsites.net/account#",
               "category" : 0,
               "name" : "pwcapp.azurewebsites.net/account#",
               "description" : null,
               "date" : "11.12.2015",
               "_id" : 2,
               "tags" : null,
               "is_favorite" : false
       },
       {
               "url" : "http://pwcapp.azurewebsites.net/account#",
               "category" : 2,
               "name" : "pwcapp.azurewebsites.net/account#",
               "description" : null,
               "date" : "11.12.2015",
               "_id" : 3,
               "tags" : [
                       {
                               "text" : "pwc"
                       }
               ],
               "is_favorite" : true
       },
       {
               "url" : "http://agar.io",
               "category" : 4,
               "name" : "agar.io",
               "description" : null,
               "date" : "12.12.2015",
               "_id" : 4,
               "tags" : null,
               "is_favorite" : false
       },
       {
               "url" : "https://www.dropbox.com/home",
               "category" : 5,
               "name" : "www.dropbox.com/home",
               "description" : null,
               "date" : "12.12.2015",
               "_id" : 5,
               "tags" : null,
               "is_favorite" : false
       },
       {
               "url" : "http://www.deezer.com",
               "category" : 5,
               "name" : "www.deezer.com",
               "description" : null,
               "date" : "12.12.2015",
               "_id" : 6,
               "tags" : [
                       {
                               "text" : "music"
                       }
               ],
               "is_favorite" : true
       },
       {
               "url" : "http://habrahabr.ru/interesting/",
               "category" : 3,
               "name" : "habrahabr.ru/interesting/",
               "description" : null,
               "date" : "12.12.2015",
               "_id" : 7,
               "tags" : null,
               "is_favorite" : false
       }
   ],
     "counter": 5,
     "bid": 7
   }}, 
      function(err) {
        if (err) throw err;
        else callback();
    });
}

function createRequestMock() {
    return requestMock;
}

module.exports.clean = clean;
module.exports.req = createRequestMock();