var router = require('express').Router();
var db = require('../config/configurator.js');
var util = require('./util.js');

var User = db.User;

// Delete user account
// DELETE /delete
router.delete('/delete', util.checkAuth, function(req, res) {
  User.remove({
    "_id": req.session.passport.user._id
  }, function(err) {
    if (err) res.sendStatus(400);
    else {
      req.logout();
      res.sendStatus(200);
    }
  });
});

module.exports = router;
