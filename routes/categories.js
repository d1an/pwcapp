var router = require('express').Router();
var categories = require('../routes/categoriesLogics');
var util = require('./util.js');

// List categories
// GET /categories
// Returns categories list
router.get('/', util.checkAuth, categories.getCategories);

// Add a category
// POST /add {name, parent}
// Returns status
router.post('/add', util.checkAuth, categories.postCategoriesAdd);

// Edit category by id
// EDIT /categories/edit/:id
router.put('/edit/:id', util.checkAuth, categories.putCategoriesEdit);

// Delete category by id
// DELETE /categories/delete/:id
router.delete('/delete/:id', util.checkAuth, categories.deleteCategories);


// Create category tree in JSON
// GET /categories/tree
// Returns a json object with a category tree
router.get('/tree', util.checkAuth, categories.getCategoriesTree);

module.exports = router;