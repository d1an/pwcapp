var db = require('../config/configurator.js');

var User = db.User;

// GET /categories
// Tested
function getCategories(req, res) {
    User.findOne({
        "_id": req.session.passport.user._id
    }, function(err, doc) {
        if (err || !doc) {
            res.sendStatus(400);
        } else {
            doc.categories.splice(1, 1);
            res.send(doc.categories);
        }
    });
}

// Function that validates parent for the category addition 
function checkParents(parent, categories, name) {
    var unique = true;
    var there = false;
    for (var i = 0; i < categories.length; i++) {
        if (categories[i]._id == parent)
            there = true;
        if ((categories[i]["parent"] == parent || (categories[i]["parent"] == null && parent == 0)) 
            && categories[i].name === name) 
            unique = false;
    }
    return (there && unique);
}

// Function that validates children for the category editing
function checkSameCategory(parent, id, categories, name) {
    var unique = true;
    for (var i = 0; i < categories.length; i++) {
        if ((categories[i]["parent"] == parent ||
            (categories[i]["parent"] == null && parent == 0) ||
            (categories[i]["parent"] == null && parent == -1)) &&
            categories[i].name === name && categories[i]._id != id)
            unique = false;
    }
    return unique;
}

// POST /add {name, parent}
// Tested
function postCategoriesAdd(req, res) {
    User.findOne({
        "_id": req.session.passport.user._id
    }, function(err, doc) {
        if (err || !doc) res.sendStatus(400);
        if (req.body.name.toString().length == 0) {
            res.sendStatus(400);
        }
        else {
            // Set fields
            var counter = doc.counter + 1;
            var parent = req.body.parent;
            var n_cat = {
                "name": req.body.name,
                "_id": counter,
                "parent": null,
                "children": []
            };

            // Check whether parent is valid
            if (!checkParents(parent, doc.categories, req.body.name)) res.sendStatus(400);
            else {
                for (var i = 0; i < doc.categories.length; i++)
                    if (doc.categories[i]._id == parent) {
                        if (parent == 0 || parent == 1) // Insert base category
                            User.update({
                                "_id": req.session.passport.user._id
                            }, {
                                $inc: {
                                    "counter": 1
                                },
                                $push: {
                                    "categories": n_cat
                                }
                            }, function(err) {
                                if (err) res.sendStatus(400);
                                else res.sendStatus(200);
                            });
                        else {
                            // Insert sub category
                            // Mongo can't update 2 fields at the same time, so we are performing 2 single update operations
                            n_cat.parent = parent;
                            User.update({
                                "_id": req.session.passport.user._id
                            }, {
                                $inc: {
                                    "counter": 1
                                },
                                $push: {
                                    "categories": n_cat
                                }
                            }, function(err) {
                                if (err) res.sendStatus(400);
                                User.update({
                                    "_id": req.session.passport.user._id,
                                    "categories._id": parent
                                }, {
                                    $push: {
                                        "categories.$.children": counter
                                    }
                                }, function(err) {
                                    if (err) res.sendStatus(400);
                                    else res.sendStatus(200);
                                });
                            });
                        }
                    }
            }
        }
    });
}

// EDIT /categories/edit/:id
// Tested
function putCategoriesEdit(req, res) {
    var id = req.params.id;

    if (id == 0) {
        res.sendStatus(400);
    }
    else {
        User.findOne({
            "_id": req.session.passport.user._id
        }, function(err, doc) {
            if (err || !doc) res.sendStatus(400);
            else {
                if (!checkSameCategory(req.body.parent, id, doc.categories, req.body.name)) {
                    res.sendStatus(400);
                    return;
                }
                //noinspection JSDuplicatedDeclaration
                for (var i = 0; i < doc.categories.length; i++) {
                    if (doc.categories[i]._id == id) {
                        req.body.parent == -1 ? doc.categories[i].parent = null : doc.categories[i].parent = req.body.parent;
                        doc.categories[i].name = req.body.name;
                        break;
                    }
                }
                //noinspection JSDuplicatedDeclaration
                for (var i = 0; i < doc.categories.length; i++) {
                    var item = doc.categories[i];
                    if (item.children != null) {
                        for (var j = 0; j < item.children.length; j++) {
                            if (item.children[j] == id) {
                                item.children.splice(j, 1);
                                break;
                            }
                        }
                    }
                }
                //noinspection JSDuplicatedDeclaration
                for (var i = 0; i < doc.categories.length; i++) {
                    if (doc.categories[i]._id == req.body.parent) {
                        doc.categories[i].children.push(id);
                        break;
                    }
                }
                User.update({"_id": req.session.passport.user._id},
                    {$set: {"categories": doc.categories}
                }, function(err) {
                    if (err) res.sendStatus(400);
                    else res.sendStatus(200);
                });
            }
        });
    }
}

// DELETE /categories/delete/:id
// Tested
function deleteCategories(req, res) {
    var id = req.params.id;
    if (id == 0) {
        res.sendStatus(400);
    }
    else {
        User.findOne({
            "_id": req.session.passport.user._id
        }, function(err, doc) {
            if (err || !doc) res.sendStatus(400);
            //noinspection JSDuplicatedDeclaration
            for (var i = 0; i < doc.categories.length; i++) {
                if (doc.categories[i]._id == id) {
                    if (doc.categories[i].children.length != 0) {
                        res.sendStatus(400, "You have children in the category");
                        return;
                    }
                    else {
                        doc.categories.splice(i, 1);
                        break;
                    }
                }
            }
            //noinspection JSDuplicatedDeclaration
            for (var i = 0; i < doc.categories.length; i++) {
                var item = doc.categories[i];
                if (item.children != null) {
                    for (var j = 0; j < item.children.length; j++) {
                        if (item.children[j] == id) {
                            item.children.splice(j, 1);
                            break;
                        }
                    }
                }
            }
            //noinspection JSDuplicatedDeclaration
            for (var i = 0; i < doc.bookmarks.length; i++) {
                if (doc.bookmarks[i].category == id) {
                    doc.bookmarks[i].category = 0;
                }
            }
            User.update({
                "_id": req.session.passport.user._id
            }, {
                $set: {
                    "categories": doc.categories,
                    "bookmarks": doc.bookmarks
                }
            }, function(err) {
                if (err) res.sendStatus(400);
                else res.sendStatus(200);
            });
        });
    }
}

function makeCategoriesTree(cats) {
    var itemsByID = [];
    cats.forEach(function(item) {
        itemsByID[item._id] = {
            name: item.name,
            _id: item._id,
            children: [],
            parentID: item.parent
        };
    });

    itemsByID.forEach(function(item) {
        if (item.parentID !== null) {
            itemsByID[item.parentID].children.push(item);
        }
    });

    var roots = itemsByID.filter(function(item) {
        return item.parentID === null;
    });
    itemsByID.forEach(function(item) {
        delete item.parentID;
    });
    
    return roots;
}


// GET /categories/tree
// Returns a json object with a category tree
// Tested
function getCategoriesTree(req, res) {
    User.findById(req.session.passport.user._id, function(err, doc) {
        if (err || !doc) 
            res.sendStatus(400);
        else
            res.send(makeCategoriesTree(doc.categories));
    });
}

module.exports.getCategories = getCategories;
module.exports.postCategoriesAdd = postCategoriesAdd;
module.exports.putCategoriesEdit = putCategoriesEdit;
module.exports.deleteCategories = deleteCategories;
module.exports.getCategoriesTree = getCategoriesTree;