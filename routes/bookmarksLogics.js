var db = require('../config/configurator.js');

var User = db.User;

// GET /bookmarks/
// Tested
function getBookmarks(req, res) {
    var userId = req.session.passport.user._id;
    User.findById(userId, function (err, user) {
        if (err)
            res.sendStatus(400);
        else
            res.send(user.bookmarks);
    })
}

// GET /bookmarks/search/:query
//
function getBookmarksSearch(req, res, next) {
    User.findOne({
        "_id": req.session.passport.user._id
    }, function (err, doc) {
        if (err || !doc) {
            res.sendStatus(400);
        }
        else {
            var r = [];
            for (var i = 0; i < doc.bookmarks.length; i++) {
                var search = req.params.query.toLowerCase();
                if (doc.bookmarks[i].url.toLowerCase().indexOf(search) >= 0 ||
                    doc.bookmarks[i].name.toLowerCase().indexOf(search) >= 0 ||
                    (doc.bookmarks[i].description != null
                    && doc.bookmarks[i].description.toLowerCase().indexOf(search) >= 0)) {
                    r.push(doc.bookmarks[i]);
                }
                if (doc.bookmarks[i].tags) {
                    doc.bookmarks[i].tags.forEach(function (item) {
                        if (item.text.toLowerCase().indexOf(search) >= 0 &&
                            r.indexOf(doc.bookmarks[i]) == -1) {
                            r.push(doc.bookmarks[i]);
                        }
                    });
                }
            }
            res.send(r);
        }
    });
}

// GET /bookmarks/tags/search/:query
//
function getBookmarksTagsSearch(req, res, next) {
    User.findOne({
        "_id": req.session.passport.user._id
    }, function (err, doc) {
        if (err || !doc) res.sendStatus(400);
        else {
            var r = [];
            var search = req.params.query.toLowerCase();
            for (var i = 0; i < doc.bookmarks.length; i++) {
                if (doc.bookmarks[i].tags != null) {
                    for (var j = 0; j < doc.bookmarks[i].tags.length; j++) {
                        var tag = doc.bookmarks[i].tags[j];
                        if (tag.text.toLowerCase().indexOf(search) >= 0 && r.indexOf(tag) == -1) {
                            r.push(tag);
                        }
                    }
                }
            }
            res.send(r);
        }
    });
}

// GET /bookmarks/category/:id
//
function getBookmarksCategory(req, res, next) {
    User.findOne({
        "_id": req.session.passport.user._id
    }, function (err, doc) {
        if (err || !doc) res.sendStatus(400);
        // Filter bookmarks by category
        var result = [];
        for (var i = 0; i < doc.bookmarks.length; i++)
            if (doc.bookmarks[i].category == req.params.id)
                result.push(doc.bookmarks[i]);
        res.send(result);
    });
}

// POST /bookmarks/add {url, category, *name, *description}
//
function postBookmarksAdd(req, res) {
    if (req.body.url.toString().length == 0 || req.body.category.toString().length == 0) {
        res.sendStatus(400);
    }
    else {
        User.findOne({
            "_id": req.session.passport.user._id
        }, function (err, doc) {
            if (err || !doc) res.sendStatus(400);
            else {
                var bid = doc.bid + 1;
                var new_b = {
                    url: req.body.url,
                    category: req.body.category,
                    name: req.body.name,
                    description: req.body.description,
                    date: new Date(),
                    _id: bid,
                    tags: req.body.tags,
                    is_favorite: false
                };

                // DB query
                User.findByIdAndUpdate({
                        "_id": req.session.passport.user._id
                    }, {
                        $inc: {
                            "bid": 1
                        },
                        $push: {
                            "bookmarks": new_b
                        }
                    },
                    function (err, doc) {
                        if (err || !doc) res.sendStatus(400);
                        else res.sendStatus(200);
                    });
            }
        });
    }
}

// POST /bookmarks/extension/add {url}
//
function postBookmarksExtensionAdd(req, res, next) {
    if (req.body.url.toString().length == 0) {
        res.sendStatus(400);
    }
    else {
        var userId = req.session.passport.user._id;
        User.findById(userId, function (err, user) {
            if (err)
                res.sendStatus(400);
            else {
                var bid = user.bid + 1;

                // Fully Qualified Domain Name 1
                // Non-greedy match on filler
                var re1 = '.*?';
                var re2 = '((?:[a-zа-я][a-zа-я\\.\\d\\-]+)\\.(?:[a-zа-я][a-zа-я\\-]+))(?![\\w\\.])';
                var p = new RegExp(re1 + re2, "i");
                var m = p.exec(req.body.url);

                var new_b = {
                    url: req.body.url,
                    category: 0,
                    name: m[1],
                    description: '',
                    date: new Date(),
                    _id: bid,
                    tags: null,
                    is_favorite: false
                };

                user.bookmarks.push(new_b);
                user.bid = bid;
                user.save(function (err) {
                    if (err) res.sendStatus(400);
                    else res.sendStatus(200);
                })
            }
        });
    }
}

// DELETE /bookmarks/delete/:bid
//
function deleteBookmarks(req, res, next) {
    var id = req.params.bid;
    // Update collection
    User.update({
        '_id': req.session.passport.user._id
    }, {
        $pull: {
            "bookmarks": {
                "_id": parseInt(id, 10)
            }
        }
    }, function (err, doc) {
        if (err || !doc) res.sendStatus(400);
        else res.sendStatus(200);
    });
}

// GET /bookmarks/favorite
//
function getFavorites(req, res, next) {
    if (req.session.passport.user) {
        // Init DB
        //var db = req.db;
        //var collection = db.get('users');
        // DB query
        User.findOne({
            "_id": req.session.passport.user._id
        }, function (err, doc) {
            if (err || !doc) res.sendStatus(400);
            else {
                var result = [];
                doc.bookmarks.forEach(function (item) {
                    if (item.is_favorite) {
                        result.push(item);
                    }
                });
                res.send(result);
            }
        });
    }
    else {
        res.sendStatus(401);
    }
}

// PUT /bookmarks/favorite/:bid
//
function setFavorite(req, res, next) {
    var id = req.params.bid;
    // DB query
    User.update({
        '_id': req.session.passport.user._id,
        'bookmarks._id': parseInt(id, 10)
    }, {
        $set: {
            'bookmarks.$.is_favorite': req.body.is_favorite
        }
    }, function (err, doc) {
        if (err || !doc) res.sendStatus(400);
        else {
            res.sendStatus(200);
        }
    });
}

// PUT /bookmarks/edit/:bid {} 
//
function putBookmarks(req, res, next) {
    var id = req.params.bid;
    if (req.body.url.toString().length == 0 || req.body.category.toString().length == 0) {
        res.sendStatus(400);
        return;
    }
    // DB query
    User.update({
        '_id': req.session.passport.user._id,
        'bookmarks._id': parseInt(id, 10)
    }, {
        $set: {
            'bookmarks.$.category': req.body.category,
            'bookmarks.$.name': req.body.name,
            'bookmarks.$.url': req.body.url,
            'bookmarks.$.description': req.body.description,
            'bookmarks.$.tags': req.body.tags
        }
    }, function (err, doc) {
        if (err || !doc) res.sendStatus(400);
        else res.sendStatus(200);
    });
}


module.exports.getBookmarks = getBookmarks;
module.exports.getBookmarksSearch = getBookmarksSearch;
module.exports.getBookmarksTagsSearch = getBookmarksTagsSearch;
module.exports.getBookmarksCategory = getBookmarksCategory;
module.exports.postBookmarksAdd = postBookmarksAdd;
module.exports.postBookmarksExtensionAdd = postBookmarksExtensionAdd;
module.exports.deleteBookmarks = deleteBookmarks;
module.exports.putBookmarks = putBookmarks;
module.exports.setFavorite = setFavorite;
module.exports.getFavorites = getFavorites;