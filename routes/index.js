var passport = require('passport');
var router = require('express').Router();
var db = require('../config/configurator.js');
var User = db.User;

// Homepage
// GET /
// Returns rendered homepage
router.get('/', function(req, res) {
   //res.render('login');
   if (req.isAuthenticated()) {
      res.redirect('/account');
   }
   else {
      res.render('login');
   }
});

// Page with personal information
// GET /account
// If user is authentificated will be rendered his categories and bookmarks
router.get('/account', ensureAuthenticated, function(req, res) {
   //var db = req.db;
   //var collection = db.get('users');

   User.findOne(req.session.passport.user, function(err, user) {
      if (err) {
         res.redirect('/');
      }
      else {
         res.render('index', {
            user: user
         });
      }
   });
});

// Get user information by session
// GET /account/info
// Returns full user data if user is authentificated
router.get('/account/info', function(req, res) {
   if (req.session.passport.user) {
      //var db = req.db;
      //var collection = db.get('users');

      User.findOne({
         "_id": req.session.passport.user._id
      }, function(err, user) {
         if (err || !user) {
            res.sendStatus(400);
         }
         else {
            var information = {
               _id: user._id,
               oauth_id: user.oauth_id,
               name: user.name
            };
            res.send(information);
         }
      });
   }
   else {
      res.sendStatus(401);
   }
});

// Get permission to read google profile information
// GET /auth/google
//noinspection JSUnresolvedFunction
router.get('/auth/google',
   passport.authenticate('google', {
      scope: [
         'https://www.googleapis.com/auth/plus.login',
         'https://www.googleapis.com/auth/plus.profile.emails.read'
      ]
   }));

// Rendering page with categories and bookmarks using callback url 
// GET /auth/google/callback
// If user provides a permission he will be redirected to success url 
//noinspection JSUnresolvedFunction
router.get('/auth/google/callback',
   passport.authenticate('google', {
      successRedirect: '/account',
      failureRedirect: '/'
   }));

// Get permission to read facebook profile information
// GET /auth/facebook
//noinspection JSUnresolvedFunction
router.get('/auth/facebook',
   passport.authenticate('facebook'));

// Rendering page with categories and bookmarks using callback url 
// GET /auth/facebook/callback
// If user provides a permission he will be redirected to success url 
//noinspection JSUnresolvedFunction
router.get('/auth/facebook/callback',
   passport.authenticate('facebook', {
      successRedirect: '/account',
      failureRedirect: '/'
   }));

// Get permission to read twitter profile information
// GET /auth/twitter
//noinspection JSUnresolvedFunction
router.get('/auth/twitter',
   passport.authenticate('twitter'));

// Rendering page with categories and bookmarks using callback url 
// GET /auth/twitter/callback
// If user provides a permission he will be redirected to success url 
//noinspection JSUnresolvedFunction
router.get('/auth/twitter/callback',
   passport.authenticate('twitter', {
      successRedirect: '/account',
      failureRedirect: '/'
   }));

// Destroy user session
// GET /logout
router.get('/logout', function(req, res) {
   req.logout();
   res.redirect('/');
});

// Function checks that user is authentificated or not
function ensureAuthenticated(req, res, next) {
   if (req.isAuthenticated()) {
      return next();
   }
   else {
      res.redirect('/');
   }
}

module.exports = router;
