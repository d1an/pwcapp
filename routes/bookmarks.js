var router = require('express').Router();
var bookmarks = require('../routes/bookmarksLogics');
var util = require('./util.js');

// Get bookmarks list for a user with specified email
// GET /bookmarks
// Returns specified user's bookmarks
router.get('/', util.checkAuth, bookmarks.getBookmarks);


// Search bookmarks
// GET /bookmarks/search/:query
// Returns the result (an array of matching bookmarks) of a search query (case-insensitive)
router.get('/search/:query', util.checkAuth, bookmarks.getBookmarksSearch);


// Search tags
// GET /bookmarks/tags/search/:query
// Returns the suggestions (an array of matching tags) of a search query (case-insensitive)
router.get('/tags/search/:query', util.checkAuth, bookmarks.getBookmarksTagsSearch);


// Get bookmarks list filtered by category
// GET /bookmarks/category/:id
// Returns a list of the bookmarks with specified category
router.get('/category/:id', util.checkAuth, bookmarks.getBookmarksCategory);


// Add bookmark (* fields are optional)
// POST /bookmarks/add {url, category, *name, *description}
// Returns updated bookmarks collection
router.post('/add', util.checkAuth, bookmarks.postBookmarksAdd);


// Add bookmark using browser extension (via Google)
// POST /bookmarks/extension/add {url}
router.post('/extension/add', util.checkAuth, bookmarks.postBookmarksExtensionAdd);


// Delete a bookmark with a given ID
// DELETE /bookmarks/delete/:bid
router.delete('/delete/:bid', util.checkAuth, bookmarks.deleteBookmarks);


// Edit a bookmark
// PUT /bookmarks/edit/:bid
router.put('/edit/:bid', util.checkAuth, bookmarks.putBookmarks);

// Mark bookmark as favorite or not
// PUT /bookmarks/favorite/:bid
router.put('/favorite/:bid', util.checkAuth, bookmarks.setFavorite);

// Search favorite bookmarks
// GET /bookmarks/favorite
router.get('/favorite', util.checkAuth, bookmarks.getFavorites);

module.exports = router;