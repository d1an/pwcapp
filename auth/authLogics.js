var User = require('../config/configurator.js').User;
//function processAuth(request, accessToken, refreshToken, profile, done) {
function processAuth(profile, accessToken, refreshToken, profile, done) {
    User.findOne({oauth_id: profile.id}, function(err, user) {
        if (err)
            console.log(err);
        else {
            if (user != null) // Existing user
                done(null, user);
            else { // Create new user
                var obj = {
                    oauth_id: profile.id,
                    name: profile.displayName,
                    email: null,
                    bookmarks: [],
                    categories: [{
                        name: "Unsorted",
                        _id: 0,
                        parent: null,
                        children: null
                    }, {
                        name: "Favorite",
                        _id: 1,
                        parent: null,
                        children: null
                    }],
                    counter: 1,
                    bid: 0
                };

                var new_user = new User(obj);

                new_user.save(function (err, doc) {
                    if (err || !doc) {
                        console.log(err);
                    }
                    else {
                        done(null, new_user);
                    }
                });
            }
        }
    });
}

module.exports.processAuth = processAuth;