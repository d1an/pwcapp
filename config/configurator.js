var dbConfig = require('./db.config.js');
var authConfig = require('./auth.config.js');

var environment = process.env.NODE_ENV;
var strategies, connection;

switch (environment) {
    case 'dev':
        strategies = authConfig.development;
        connection = dbConfig.development;
        break;
    case 'prod':
        strategies = authConfig.production;
        connection = dbConfig.production;
        break;
    case 'test':
        strategies = null; // A very dangerous place, don't try this at home
        connection = dbConfig.testing;
        break;
    default:
        strategies = authConfig.production;
        connection = dbConfig.production;
        break;
}

var db = require('mongoose').connect(connection);
var userSchema = require('../models/user.js').UserSchema;
var userModel = db.model('users', userSchema);

module.exports.strategies = strategies;
module.exports.User = userModel;