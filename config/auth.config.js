module.exports = {
    // Production tokens for azure hosting
    // Don't forget to change tokens when hosting on your own server
    // WARNING! Production server (pwcapp.azurewebsites.net) is shut down forever
    // New server: http://ipwc.azurewebsites.net/
    production: {
        // Google tokens are working properly, yay!
        google: {
            'clientID': '532375305078-gmk2dfp92vt3gbph0pq0aabndjj0trds.apps.googleusercontent.com',
            'clientSecret': 'CGNEuKbPznsmhnwAyZp_Tk5e',
            'callbackURL': 'http://ipwc.azurewebsites.net/auth/google/callback'
        },
        // FB and Twitter are obsolete
        facebook: {
            'clientID': '137678346593236',
            'clientSecret': '377bf71e34e5245e837146b114c5cfa4',
            'callbackURL': 'http://pwcapp.azurewebsites.net/auth/facebook/callback'
        },
        twitter: {
            'consumerKey': 'tJ1fDXoPmNiEb4m5w4IBkMfMV',
            'consumerSecret': 'Cx8WXEsVyAkoXcgwP3EBvDY56KgJz6fafjmonW798P9uwEpnCZ',
            'callbackURL': 'http://pwcapp.azurewebsites.net/auth/twitter/callback'
        }
    },
    // Development tokens (for localhost:3000)
    // IMPORTANT! Facebook and Twitter tokens don't work by the moment
    development: {
        google: {
            'clientID': '532375305078-g4jk9clb9gvua8gl8jsnnjadbtotahn6.apps.googleusercontent.com',
            'clientSecret': 'DXgjzTWxa2-TO3POLr8ngDDv',
            'callbackURL': 'http://localhost:3000/auth/google/callback'
        },
        facebook: {
            'clientID': '645379625602537',
            'clientSecret': 'f2049172e1137656d0dfcf385874abff',
            'callbackURL': 'https://pwcapp-d1an-1.c9.io/auth/facebook/callback'
        },
        twitter: {
            'consumerKey': 'ug9HFMCTDbhnkQtiyPke7qwkd',
            'consumerSecret': '5yR5OwPch5nPqXSHEQBPbxTroSrTai3TxARr31Wdz5pPYDjICL',
            'callbackURL': 'https://pwcapp-d1an-1.c9.io/auth/twitter/callback'
        }
    }
};