var pwcFilters = angular.module('pwcFilters', []);
pwcFilters.filter('faviconUrl', function() {
   return function(text) {
      var re = /[а-я\w]+:\/\/([а-я\w\-](\.)?)+/i;
      var array = re.exec(text);
      return array[0];
   };
});
pwcFilters.filter('domainURL', function() {
   return function(text) {
      var re1='.*?';	// Non-greedy match on filler
      var re2='((?:[a-zа-я][a-zа-я\\.\\d\\-]+)\\.(?:[a-zа-я][a-zа-я\\-]+))(?![\\w\\.])';	// Fully Qualified Domain Name 1
      
      var p = new RegExp(re1+re2,["i"]);
      var m = p.exec(text);
      if (m != null)
        return m[1];
   };
});
pwcFilters.filter('searchFor', function() {
   return function(array, searchInput) {
      if (!searchInput) {
         return array;
      }
      searchInput = searchInput.toLowerCase();
      var result = [];
      angular.forEach(array, function(bookmark) {
         if (bookmark.name.toLowerCase().indexOf(searchInput) >= 0 ||
            bookmark.url.toLowerCase().indexOf(searchInput) >= 0 ||
            (bookmark.description && bookmark.description.toLowerCase().indexOf(searchInput) >= 0)) {
            result.push(bookmark);
         }
         if (bookmark.tags) {
            angular.forEach(bookmark.tags, function(tag) {
               if (tag.text.toLowerCase().indexOf(searchInput) >=0 && result.indexOf(bookmark) == -1) {
                  result.push(bookmark);
               }
            })
         }
      });
      return result;
   };
});