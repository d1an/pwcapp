var pwcServices = angular.module('pwcServices', []);

pwcServices.service("BookmarkService", function($http) {

   this.save = function(bookmark) {
      return $http.post('/bookmarks/add', bookmark);
   };

   this.getByCategoryId = function(id) {
      return $http.get('/bookmarks/category/' + id);
   };

   this.search = function(query) {
      return $http.get('/bookmarks/search/' + query);
   };

   this.setFavorite = function(id, isFavorite) {
      return $http.put('/bookmarks/favorite/' + id, isFavorite);
   };

   this.getFavorites = function() {
      return $http.get('/bookmarks/favorite');
   };

   this.searchTags = function(query) {
      return $http.get('/bookmarks/tags/search/' + query);
   };

   this.edit = function(id, bookmark) {
      return $http.put('/bookmarks/edit/' + id, bookmark);
   };

   this.delete = function(id) {
      return $http.delete('/bookmarks/delete/' + id);
   };
});

pwcServices.service("CategoryService", function($http) {

   this.save = function(category) {
      return $http.post('/categories/add', category);
   };

   this.getAll = function() {
      return $http.get('/categories');
   };

   this.getTree = function() {
      return $http.get('/categories/tree');
   };

   this.delete = function(id) {
      return $http.delete('/categories/delete/' + id);
   };

   this.edit = function(id, category) {
      return $http.put('/categories/edit/' + id, category);
   };

});

pwcServices.service("UserService", function($http) {

   this.getInfo = function() {
      return $http.get('/account/info');
   };

   this.delete = function() {
      return $http.delete('/users/delete');
   };
});