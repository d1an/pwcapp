var pwcModule = angular.module('pwcModule', ['angularUtils.directives.dirPagination', 'pwcDirectives', 'pwcFilters', 'pwcServices', 'ngTagsInput', 'ui.tree', 'ngDragDrop', 'youtube-embed']);

pwcModule.controller('AccountController', ['$scope', '$http', '$timeout', '$window', 'BookmarkService', 'CategoryService', 'UserService', function($scope, $http, $timeout, $window, BookmarkService, CategoryService, UserService) {

    // Get initial information with unsorted bookmarks as default
    UserService.getInfo().success(function(data) {
        $scope.userInformation = data;
        CategoryService.getAll().success(function(data) {
            $scope.categories = data;
            $scope.searchInput = "";
            $scope.addFormData = {};
            $scope.openedCategory = 0;
            // flag to control filled name when user writes url
            $scope.nameFlag = false;
            // flag to prevent fast clicks on the add/edit buttons
            $scope.operationFlag = false;
            $scope.editFormData = {};
            //$scope.optionsCategories = [];
            $scope.orderOptions = [{
                id: 'date',
                text: 'by date'
            }, {
                id: 'name',
                text: 'by name'
            }];

            // init order direction
            $scope.orderDirection = true;
            // init order by date as default
            $scope.order = $scope.orderOptions[0].id;

            // Get categories tree to build panel "My categories"
            CategoryService.getTree().success(function(data) {
                $scope.categoriesTree = data;
                BookmarkService.getByCategoryId(0).success(function(data) {
                    $scope.currentBookmarks = data;
                    $scope.bookmarks = data;
                    $scope.currentCategory = 0;
                    // array to build path of current category
                    document.getElementById($scope.currentCategory).classList.add("selected-category");
                    $scope.path = [];
                    $scope.path.push(findCategoryById($scope.currentCategory));
                }).error(function() {
                    console.log('Cant get bookmarks by id');
                });
            }).error(function() {
                console.log('Cant get categories tree');
            });
        }).error(function() {
            console.log('Cant get categories');
        });
    }).error(function() {
        console.log('Cant get user info');
    });

    // Array that represents existing suggestions during writing the tags
    // will be uploaded after writing min 3 leters
    $scope.loadTags = function(query) {
        return BookmarkService.searchTags(query).success(function(data) {
            console.log('successful');
        }).error(function() {
            console.log('Cant get tags');
        });
    };

    // Add new bookmark
    $scope.addBookmark = function() {
        if ($scope.operationFlag) {
            return;
        }
        $scope.operationFlag = true;
        var bookmark = {
            name: $scope.addFormData.name,
            url: $scope.addFormData.url,
            category: $scope.addFormData.category,
            description: $scope.addFormData.comment,
            tags: $scope.addFormData.selectedTags
        };
        BookmarkService.save(bookmark).success(function(data, status, headers, config) {
            $scope.addFormData = {};
            $('#collapseAdd').collapse('hide');
            $('#bookmarkModal').modal('hide');
            if (status == 400) {
                swal("Operation failed!", "New bookmark hasn't been added!", "error");
            }
            $scope.nameFlag = false;
            $scope.operationFlag = false;
            refreshBookmarks();
            checkSearch();
        }).error(function() {
            console.log('Cant add bookmark');
            $scope.operationFlag = false;
        });
    };

    // Set bookmark as favorite or not
    $scope.setFavorite = function(id, is_favorite) {
        var data = {
            is_favorite: is_favorite
        };
        BookmarkService.setFavorite(id, data).success(function(data, status, headers, config) {
            $scope.operationFlag = false;
            checkSearch();
        }).error(function(data, status, headers, config) {
            console.log('Cant mark bookmark');
            $scope.operationFlag = false;
            if (status == 400) {
                swal("Operation failed!", "Bookmark was not been marked!", "error");
            }
        });
    };

    // Edit bookmark
    $scope.editBookmark = function() {
        if ($scope.operationFlag) {
            return;
        }
        $scope.operationFlag = true;
        var bookmark = {
            name: $scope.editFormData.name,
            url: $scope.editFormData.url,
            description: $scope.editFormData.comment,
            category: $scope.editFormData.category,
            tags: $scope.editFormData.selectedTags
        };
        BookmarkService.edit($scope.editBook._id, bookmark).success(function(data, status, headers, config) {
            console.log('bookmark edited');
            $scope.editFormData = {};
            $('#collapseEdit').collapse('hide');
            $('#editBookmarkModal').modal('hide');
            if (status == 400) {
                swal("Operation failed!", "Bookmark hasn't been edited!", "error");
            }
            $scope.operationFlag = false;
            checkSearch();
        }).error(function() {
            console.log('Cant edit bookmark');
            $scope.operationFlag = false;
        });
    };

    // Open bookmarkModal
    $scope.openBookmarkModal = function() {
        $scope.addTagText = '';
        $scope.addFormData.category = $scope.currentCategory;
        $('#bookmarkModal').modal('show');
    };

    // Open editCategoryModal
    $scope.openEditCategoryModal = function() {
        $scope.editNameCategory = findCategoryById($scope.currentCategory).name;
        $scope.categoriesForEdit = [{
            name: 'First level',
            _id: -1
        }];
        var cat = findCategoryById($scope.currentCategory);
        if(cat.parent){
            $scope.newCategory = cat.parent;
        }
        else{
            $scope.newCategory = -1;
        }
        getCategoriesForEdit($scope.categoriesTree, $scope.currentCategory);
        $('#editCategoryModal').modal('show');
    };

    // Open categoryModal
    $scope.openCategoryModal = function(id) {
        $scope.openedCategory = id;
        $('#categoryModal').modal('show');
    };

    $scope.openUrl = function(url){
        $window.open(url, '_blank');
    };

    // Add new category
    $scope.addCategory = function(parent) {
        if ($scope.operationFlag) {
            return;
        }
        $scope.operationFlag = true;
        var category = {
            name: $scope.nameCategory,
            parent: $scope.openedCategory
        };

        CategoryService.save(category).success(function(data, status, headers, config) {
            console.log('category added');
            $scope.nameCategory = '';
            $('#categoryModal').modal('hide');
            $scope.operationFlag = false;
            refreshCategories();
        }).error(function(data, status, headers, config) {
            console.log('Cant add category');
            $scope.operationFlag = false;
            if (status == 400) {
                $('#categoryModal').modal('hide');
                swal("Operation failed!", "Category with same name already exist here!", "error");
            }
        });
    };

    // Delete category by id
    $scope.deleteCategoryById = function(id) {
        swal({
            title: "Are you sure?",
            text: "After deleting you couldn't recover this category, all bookmarks will be moved to 'Unsorted'",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "rgb(149,209,149)",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function(isConfirm) {
            if (isConfirm) {
                CategoryService.delete(id).success(function(data, status, headers, config) {
                    refreshCategories();
                    $scope.currentCategory = 0;
                    $scope.getBookmarks(findCategoryById(0));
                    swal("Deleted", "This category has been deleted.", "success");
                }).error(function(data, status, headers, config) {
                    if (status == 400) {
                        swal("Operation failed!", "Please, delete all subcategories!", "error");
                        return;
                    }
                    console.log('Cant delete category');
                });
            }
        });
    };

    // Edit category
    $scope.editCategory = function() {
        if ($scope.operationFlag) {
            return;
        }
        $scope.operationFlag = true;
        var category = {
            name: $scope.editNameCategory,
            parent: $scope.newCategory
        };
        CategoryService.edit($scope.currentCategory, category).success(function(data, status, headers, config) {
            console.log('category edited');
            $scope.editFormData = {};
            $scope.operationFlag = false;
            refreshCategories();
            $scope.getBookmarks(findCategoryById($scope.currentCategory));
            $('#editCategoryModal').modal('hide');
        }).error(function(data, status, headers, config) {
            console.log('Cant edit category');
            $scope.operationFlag = false;
            if (status == 400) {
                $('#editCategoryModal').modal('hide');
                swal("Operation failed!", "The category with same name already exist there!", "error");
            }
        });
    };

    // Get content for div with path or search result text
    $scope.getContent = function() {
        if ($scope.searchInput.length == 0) {
            document.getElementById("pathElem").className = "tab-pane fade in active";
            document.getElementById("search-element").className = "tab-pane fade";
            document.getElementById("searchEverywhere").className = "tab-pane fade";
            $scope.bookmarks = $scope.currentBookmarks;
        }
        else if (!$scope.isEverywhere && $scope.searchInput.length > 0) {
            document.getElementById("pathElem").className = "tab-pane fade";
            document.getElementById("search-element").className = "tab-pane fade in active";
            document.getElementById("searchEverywhere").className = "tab-pane fade";
            $scope.bookmarks = $scope.currentBookmarks;
        }
        else if ($scope.isEverywhere && $scope.searchInput.length > 0) {
            document.getElementById("pathElem").className = "tab-pane fade";
            document.getElementById("search-element").className = "tab-pane fade";
            document.getElementById("searchEverywhere").className = "tab-pane fade in active";
        }
    };
    
    var timer = false;
    $scope.$watch('searchInput', function() {
        if (timer) {
            $timeout.cancel(timer);
        }
        if ($scope.isEverywhere && $scope.searchInput.length > 0) {
            timer = $timeout(function() {
                BookmarkService.search($scope.searchInput).success(function(data) {
                    $scope.bookmarks = data;
                }).error(function() {
                    console.log('Cant get search result');
                });
            }, 600);
        }
    });

    // Delete bookmark
    $scope.deleteBookmarkById = function(id) {
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this bookmark!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "rgb(149,209,149)",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function(isConfirm) {
            if (isConfirm) {
                BookmarkService.delete(id).success(function(data) {
                    checkSearch();
                    swal({
                        title: "Deleted",
                        text: "Your bookmark has been deleted.",
                        type: "success",
                        timer: 1000,
                        showConfirmButton: true
                    });
                }).error(function() {
                    console.log('Cant delete bookmark');
                });
            }
        });
    };

    // Check if it is a video url
    $scope.checkUrlOnYoutube = function(text) {
        var re2 = '(?:youtube(?:-nocookie)?\.com\/(?:[^/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?/ ]{11})'; // video id
        var p = new RegExp(re2, ["i"]);
        var matches = text.match(p);
        if (matches != null)
            return matches[1];
        else
            return null;
    };

    // Initialize data for getBookmarks
    function initBookmarks(data, category) {
        document.getElementById($scope.currentCategory).classList.remove("selected-category");
        $scope.searchInput = "";
        $scope.isEverywhere = false;
        $scope.getContent();
        if (document.getElementById("inputSer").classList.contains('zbz-input-clearable__x')) {
            document.getElementById("inputSer").classList.remove('zbz-input-clearable__x');
        }
        $scope.currentBookmarks = data;
        $scope.bookmarks = data;
        $scope.currentCategory = category._id;
        document.getElementById($scope.currentCategory).classList.add("selected-category");
        getCurrentPath();
    }

    // Get bookmarks by category
    // For onclick in html
    $scope.getBookmarks = function(category) {
        if (category._id == 1) {
            BookmarkService.getFavorites().success(function(data) {
                initBookmarks(data, category);
            }).error(function() {
                console.log('Cant get bookmark by category');
            });
        }
        else {
            BookmarkService.getByCategoryId(category._id).success(function(data) {
                initBookmarks(data, category);
            }).error(function() {
                console.log('Cant get bookmark by category');
            });
        }
    };

    // Get bookmark for editBookmarkModal
    $scope.getCurrentBook = function(id) {
        $scope.editTagText = '';
        for (var i = 0; i < $scope.bookmarks.length; i++) {
            if ($scope.bookmarks[i]._id == id)
                $scope.editBook = $scope.bookmarks[i];
        }
        $scope.editFormData = {
            name: $scope.editBook.name,
            url: $scope.editBook.url,
            comment: $scope.editBook.description,
            category: $scope.editBook.category,
            selectedTags: $scope.editBook.tags
        };
        $('#editBookmarkModal').modal('show');
    };

    // Delete this account
    $scope.deleteUser = function() {
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this account!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "rgb(149,209,149)",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function(isConfirm) {
            if (isConfirm) {
                UserService.delete().success(function() {
                    $window.location = '/';
                }).error(function() {
                    console.log('Cant delete account');
                });
            }
        });
    };

    // Get new data of current page
    function refreshBookmarks() {
        if ($scope.currentCategory == 1) {
            BookmarkService.getFavorites().success(function(data) {
                $scope.currentBookmarks = data;
                $scope.bookmarks = data;
            }).error(function() {
                console.log('Cant refresh bookmarks');
            });
        }
        else {
            BookmarkService.getByCategoryId($scope.currentCategory).success(function(data) {
                $scope.currentBookmarks = data;
                $scope.bookmarks = data;
            }).error(function() {
                console.log('Cant refresh bookmarks');
            });
        }
    }

    // Update categories
    function refreshCategories() {
        CategoryService.getTree().success(function(data) {
            $scope.categoriesTree = data;
        }).error(function() {
            console.log('Cant refresh categories tree');
        });
        CategoryService.getAll().success(function(data) {
            $scope.categories = data;
            getCurrentPath();
            document.getElementById($scope.currentCategory).classList.add("selected-category");
        }).error(function() {
            console.log('Cant refresh categories');
        });
    }

    // Find category by id (for building path)
    function findCategoryById(id) {
        for (var i = 0; i < $scope.categories.length; i++) {
            if ($scope.categories[i]._id == id) {
                return $scope.categories[i];
            }
        }
        return null;
    }

    // Build path of current category
    function recPath(id) {
        var cat = findCategoryById(id);
        $scope.path.push(cat);
        if (cat.parent != null) {
            return recPath(cat.parent);
        }
        return false;
    }

    // Build array with categories without Unsorted and Favorite
    // for example, we have: 1.Music -> 1.1.Pop -> 1.1.1.Kesha, 1.2.Rock; 2.Video
    // so list will be: Music, Pop, Kesha, Rock, Video (category parent and after his children)
    // we 
    function getCategoriesForEdit(tree, currentId) {
        angular.forEach(tree, function(item) {
            if (item._id != 0 && item._id != 1 && item._id != currentId) {
                $scope.categoriesForEdit.push(item);
                getCategoriesForEdit(item.children, currentId);
            }
            else {
                return false;
            }
        });
    }

    // We will get list of categories [Kesha, Pop, Music] and we need to reverse array
    function getCurrentPath() {
        if ($scope.currentCategory == 1) {
            $scope.path = [{
                name: 'Favorite',
                _id: '1'
            }];
            return;
        }
        $scope.path = [];
        recPath($scope.currentCategory);
        $scope.path.reverse();
    }

    // Fill name of the bookmark if user entered url
    $scope.fillName = function() {
        if ($scope.addFormData.url.indexOf("://") > 0) {
            var array = $scope.addFormData.url.split("://");
            var tempStr = array[1];
            if (!$scope.nameFlag) {
                $scope.addFormData.name = tempStr;
            }
        }
    };
    
    function checkSearch(){
        if ($scope.isEverywhere && $scope.searchInput.length > 0) {
                BookmarkService.search($scope.searchInput).success(function(data) {
                    $scope.bookmarks = data;
                }).error(function() {
                    console.log('Cant get search result');
                });
        } else{
            refreshBookmarks();
        }
    };

    // Handle drop operation
    $scope.onDrop = function(target, source) {
        if (target._id == 1) {
            swal("Operation failed!", "Bookmark can't be moved to 'Favorite', so just click on ❤", "warning");
            return;
        }
        if (target._id == source.category) {
            swal({
                title: "Bookmark is already here!",
                text: "",
                timer: 1000,
                showConfirmButton: true
            });
            return;
        }
        var bookmark = {
            name: source.name,
            url: source.url,
            description: source.description,
            category: target._id,
            tags: source.tags
        };
        BookmarkService.edit(source._id, bookmark).success(function(data, status, headers, config) {
            console.log('bookmark edited');
            swal({
                title: "Moved",
                text: "Your bookmark has been moved.",
                type: "success",
                timer: 1000,
                showConfirmButton: true
            });
            refreshBookmarks();
        }).error(function(data, status, headers, config) {
            if (status == 400) {
                swal("Operation failed!", "Bookmark hasn't been edited!", "error");
            }
            console.log('Cant edit bookmark');
        });
    };
}]);